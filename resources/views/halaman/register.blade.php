<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
    <form action="/submit" method="POST">
    @csrf_field

        <label> First Name :</label><br>
        <input type="text" name="nama_depan"><br><br>
        <label> Last Name :</label><br>
        <input type="text" name="nama_belakang"><br><br>
        
        <label> Gender</label><br>
        <input type="radio" name="WN"> Man<br>
        <input type="radio" name="WN"> Woman<br>
        <input type="radio" name="WN"> Other<br><br>

        <label> Nationality</label>
        <select name="JK">
            <option value="1"> Indonesia</option>
            <option value="2"> Singapore</option>
            <option value="3"> Malaysia</option>
            <option value="4"> Japanese</option>
        </select><br><br>

        <label> Languege Speake</label><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Arabic<br>
        <input type="checkbox"> Japanese <br><br>
        <label> Bio</label> <br>
        <textarea name="alamat" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="submit">
    </form>
</body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function submit(Request $request){
        $nama = $request['nama_depan'];
        $nama2 = $request['nama_belakang'];

        return view('halaman.welcome', compact('nama', 'nama2'));
    }
}
